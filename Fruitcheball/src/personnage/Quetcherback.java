package personnage;

import fruit.Fruit;
import labyrinthe.Case;
import labyrinthe.CaseVide;
import labyrinthe.Labyrinthe;

public class Quetcherback extends Personnage {

	public Quetcherback(Case p) {
		super(p);
		// TODO Auto-generated constructor stub
	}
	
	public Quetcherback(Case p, Fruit f) {
		super(p, f);
		// TODO Auto-generated constructor stub
	}

	public String lancer(char c) {
		return super.lancer(c);
	}
	
	@Override
	public String recolter() {
		Fruit objet_case = this.position.possederUnFruit();
		if(objet_case!=null){
			((CaseVide) this.position).retirerFruit();
			return "P";
		}
		return "";
	}
	
	@Override
	public void intercepter(Fruit f) {
		if(this.inventaireVide()){
			this.inventaire=f;
		}
	}

	@Override
	public char seDeplacer(Labyrinthe l) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString() {
		return "Q";
	}
}
