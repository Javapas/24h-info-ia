package personnage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fruit.Fruit;
import labyrinthe.Case;
import labyrinthe.CaseDepart;

// G�re une �quipe
public class Equipe {

	// Attributs
	/**
	 * numero d'�quipe
	 * score de l'�quipe
	 * la liste des joueurs
	 * la liste des cases des joueurs 
	 * la liste des fruits ramass�es
	 * Etre une �quipe alli�e 
	 */
	private int numero;
	private int score;
	private Personnage[] joueurs;
	private CaseDepart[] casesDepart;
	private HashMap<Fruit,Integer> fruitsRamenes;
	private boolean alliee;
	
	private int NOMBRE_DE_JOUEUR=0;
	private int NOMBRE_DE_CASE_DEPART=0;
	
	/**
	 * Constructeur
	 * @param num , num�ro de l'�quipe
	 * @param allie , bool�en qui pr�cise si c'est l'�quipe alli�
	 */
	public Equipe(int num,boolean allie){
		this.numero=num;
		this.score=0;
		this.joueurs=new Personnage[3];
		this.casesDepart= new CaseDepart[3];
		this.fruitsRamenes=new HashMap<Fruit,Integer>();
		this.alliee=allie;
	}

	/**
	 * Retourne le num�ro d'�quipe
	 * @return
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * D�finie le num�ro d'�quipe
	 * @param numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * Retourne le score
	 * @return
	 */
	public int getScore() {
		return score;
	}

	/**
	 * D�fini le score
	 * @param score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Retourne le tableau des joueurs
	 * @return
	 */
	public Personnage[] getJoueurs() {
		return joueurs;
	}

	/**
	 * D�finie le tableau des joueurs
	 * @param joueurs
	 */
	public void setJoueurs(Personnage[] joueurs) {
		this.joueurs = joueurs;
	}

	/**
	 * R�cup�re le tableau des cases de d�part
	 * @return
	 */
	public CaseDepart[] getCasesDepart() {
		return casesDepart;
	}

	/**
	 * D�finie le tableau des cases d�part
	 * @param casesDepart
	 */
	public void setCasesDepart(CaseDepart[] casesDepart) {
		this.casesDepart = casesDepart;
	}

	/**
	 * Retourne la liste des fruits ramen�s
	 * @return
	 */
	public HashMap<Fruit,Integer> getFruitsRamenes() {
		return fruitsRamenes;
	}
	
	/**
	 * V�rifie s'il s'agit d'un alli�
	 * @return
	 */
	public boolean isAlliee() {
		return alliee;
	}

	/**
	 * Definir si c'est une �quipe alli�e
	 * @param alliee
	 */
	public void setAlliee(boolean alliee) {
		this.alliee = alliee;
	}
	
	public boolean estCaseDepart(Case c) {
		boolean trouve = false;
		for(Case ca : casesDepart) {
			if(ca.equals(c)) {
				trouve = true;
			}
		}
		return trouve;
	}
	
	/**
	 * Ajoute un joueur � la liste si celle-ci n'est pas d�j� compl�te
	 * @param j
	 */
	public void ajouterJoueur(Personnage p){
		if(this.NOMBRE_DE_JOUEUR<=3){
			this.joueurs[this.NOMBRE_DE_JOUEUR]=p;
			this.NOMBRE_DE_JOUEUR++;
		}
	}
	
	/**
	 * Ajoute une case de d�part � la liste si celle-ci n'est pas d�j� compl�te
	 * @param c
	 */
	public void ajouterCaseDepart(CaseDepart c){
		if(this.NOMBRE_DE_CASE_DEPART<=3){
			this.casesDepart[this.NOMBRE_DE_CASE_DEPART]=c;
			this.NOMBRE_DE_CASE_DEPART++;
		}
	}
	
	/**
	 * V�rifie si la liste des case est compl�te
	 * @return
	 */
	public boolean etreCompleteCaseDepart(){
		int index=this.emplacementLibre(this.casesDepart);
		if(index==-1) return false;
		return true;
	}
	
	/**
	 * V�rifie si la liste des joueurs est compl�te
	 * @return
	 */
	public boolean etreCompleteEquipe(){
		int index=this.emplacementLibre(this.joueurs);
		if(index==-1) return false;
		return true;
	}
	
	/**
	 * Retourne un emplacement libre dans la liste des joueurs
	 * Si la liste est compl�te, retourne -1, sinon retourne l'index de l'emplacement libre
	 * @return
	 */
	public int emplacementLibre(Object[] o){
		int i=0;
		int index=-1;
		while((i<o.length) &&  index==-1){
			if(this.joueurs[i]==null){
				index=i;
			} else {
				i++;
			}
		}
		return index;
	}
	
	/**
	 * Ajouter un fruit dans la liste des fruits ramen�s
	 * @param f
	 */
	public void ajouterFruit(Fruit f, int valeur){
		this.fruitsRamenes.put(f, valeur);
	}
	
	public String toString() {
		String s =  numero + " ("+alliee+") \n";
		s += "CASES DEPART :\n";
		for(Case c : casesDepart) {
			s+= c.toString() + c.getPosition() + "\n";
		}
		s+= "JOUEURS \n";
		for(Personnage p : joueurs) {
			s+= p.toString() + "\n";
		}
		s+= "SCORE : "+score + "\n";
		s+="FRUITS : \n";
        for(Fruit key : fruitsRamenes.keySet()) {
            s+= key.toString() + " : " + fruitsRamenes.get(key);
        }
		return s;
	}
}
