package personnage;

import java.util.ArrayList;

import fruit.Chataigne;
import fruit.Fruit;
import labyrinthe.*;

/**
 * Classe Personnage
 */
public abstract class Personnage {

	/**
	 * Attributs position : emplacement du personnage dans le labyrinthe
	 * inventaire : Fruit tenu par le joueur
	 */
	protected Case position;
	protected Fruit inventaire;

	/**
	 * Constructeur
	 * 
	 * @param p
	 */
	public Personnage(Case p) {
		this.position = p;
		this.inventaire = null;
	}

	/**
	 * Constructeur
	 * 
	 * @param p
	 * @param f
	 */
	public Personnage(Case p, Fruit f) {
		this.position = p;
		this.inventaire = f;
	}

	/**
	 * methode controlant le deplacement du personnage
	 * 
	 * @param l
	 *            labyrinthe dans lequel le personnage se deplace
	 */
	public abstract char seDeplacer(Labyrinthe l);

	public Case trouverFruitPlusProche(Labyrinthe l) {
		ArrayList<Case> casesFruits = new ArrayList<Case>();
		Case[][] laby = l.getCases();
		// recherche des fruits dans le labyrinthe
		for (int ligne = 0; ligne < laby.length; ligne++) {
			for (int colonne = 0; colonne < laby[0].length; colonne++) {
				if (laby[ligne][colonne].possederUnFruit() != null) {
					casesFruits.add(laby[ligne][colonne]);
					if(this instanceof Lanceur && laby[ligne][colonne].possederUnFruit().etreChataigne()) {
						casesFruits.remove(laby[ligne][colonne]);
					}
				}
			}
		}

		// recherche du fruit le plus proche

		Case caseLaPlusProche = null;
		int minimum = -1;

		for (Case c : casesFruits) {
			int[][] pathFinder = l.getPathFinder(c);
			int distance = pathFinder[(int) this.position.getPosition().getX()][(int) this.position
			                                                                    .getPosition().getY()];
			if (minimum == -1 || distance < minimum) {
				caseLaPlusProche = c;
				minimum = distance;
			}
		}

		return caseLaPlusProche;
	}

	/**
	 * Lance le fruit tenu par le joueur � 4 case de distance et dans une direction donn�e
	 * @param direction char de la direction
	 */
	public String lancer(char direction) {
		switch(direction){
		case 'N':
			// -1 en y tab[x][y]
			return "LN";
		case 'E':
			// +1 en x
			return "LE";
		case 'S':
			// +1 en y
			return "LS";
		case 'O':
			// -1 en x
			return "LO";
		default:
			return "";
		}
	}

	/**
	 * Recolte le fruit de la case o� est le joueur
	 */
	public abstract String recolter();

	/**
	 * V�rifie si l'inventaire est vide
	 */
	public boolean inventaireVide(){
		if(this.inventaire==null) return true;
		else return false;
	}

	/**
	 * Le joueur est sur le lancer d'un autre joueur et r�cup�re le fruit en question
	 * @param Fruit f
	 */
	public abstract void intercepter(Fruit f) ;

	public Case getPosition() {
		return position;
	}

	public void setPosition(Case position) {
		this.position = position;
	}

	public Fruit getInventaire() {
		return inventaire;
	}

	public void setInventaire(Fruit inventaire) {
		this.inventaire = inventaire;
	}


	public String toString() {
		return this.toString() + position.getPosition() + " : "+ inventaire;
	}
}
