package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class Traitement {
	private BufferedReader in;
	private Socket socket;
	private OutputStreamWriter out;
	
	public Traitement(String adresse, int port) throws UnknownHostException, IOException {
		this.socket=new Socket(adresse,port);
		this.in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.out= new OutputStreamWriter(socket.getOutputStream());
		
	}
	
	
	public String attendreRep() throws IOException {
		return this.in.readLine();
	}
	
	public void envoyerRep(String s) throws IOException {
		this.out.write(s);
		this.out.flush();
	}
}
