package main;

import java.util.Random;

import labyrinthe.Labyrinthe;
import personnage.Equipe;
import personnage.Personnage;

public class IATitou {

	public String message(Labyrinthe l) {
		String[] result = new String[3];
		
		Equipe allie=null;
		for(Equipe e : l.getEquipes()) {
			if(e.isAlliee()) {
				allie = e;
			}
		}
		Random rand= new Random();
		int persoEncours=0;
		int dir=0;
		for(Personnage p : allie.getJoueurs()) {
			if(p.inventaireVide()) {
				dir = rand.nextInt(3);
				switch(dir) {
				case 0:
					result[persoEncours]="N";
					break;
				case 1:
					result[persoEncours]="E";
					break;
				case 2:
					result[persoEncours]="S";
					break;
				case 3:
					result[persoEncours]="O";
					break;
				}
			}
			persoEncours++;
		}
		
		return "";
		
		
		
	}
		
}
