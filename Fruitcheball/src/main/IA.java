package main;

import java.util.ArrayList;

import labyrinthe.Case;
import labyrinthe.Labyrinthe;
import personnage.Equipe;
import personnage.Personnage;

public class IA {

	private Labyrinthe laby;

	public IA(Labyrinthe l) {
		this.laby = l;
	}

	public String deplacerEquipe(Equipe e) {
		String message="";
		for (Personnage p : e.getJoueurs()) {
			if(!message.equals("")) message+="-";
			System.out.println("D�placer joueur " + p);
			message+=this.deplacerJoueur(p,e);
		}
		return message;
		
	}

	public char deplacerJoueur(Personnage p, Equipe e) {
		char direction='N';
		
		
		if (p.getInventaire() == null) {	
			Case destination = p.trouverFruitPlusProche(this.laby);
			direction = this.getChemin(p, destination);
			System.out.println(direction);
		}
		else {
			if(p.getPosition().possederUnFruit()!=null && !(p.getPosition().possederUnFruit().etreChataigne())) {
				direction = 'P';
			}
			else if(p.getPosition().possederUnFruit()!=null && e.estCaseDepart(p.getPosition())) {
				direction = 'P';
			}
		else if (!p.getInventaire().etreChataigne()) {
				Case[] casesDeparts = e.getCasesDepart();
				Case destination = getCasePlusProche(p,casesDeparts);
				direction = this.getChemin(p, destination);
			}
		}
		
		return direction;
	}
	
	private Case getCasePlusProche(Personnage p,Case[] listeCases) {
		Case casePlusProche = null;
		int minimum = -1;
		for (Case c : listeCases){
			int[][] pathFinder = this.laby.getPathFinder(c);
			int distance  = pathFinder[(int) p.getPosition().getPosition().getX()][(int) p.getPosition().getPosition().getY()];
			if (minimum == -1 || minimum>distance) {
				minimum=distance;
				casePlusProche=c;
			}
		}
		return casePlusProche;
	}

	public char getChemin(Personnage p, Case destination){
		char direction = 'N';
		Case pos = p.getPosition();
		Case prochaine_case=null;
		int[][] pathFinder = this.laby.getPathFinder(destination);
		int xPersonnage = (int) pos.getPosition().getX();
		int yPersonnage = (int) pos.getPosition().getY();
		int distance = pathFinder[xPersonnage][yPersonnage];
		String res="";
		
		for (int i = 0; i < pathFinder.length; i++) {
			for (int j = 0; j < pathFinder[0].length; j++) {
				res+=pathFinder[i][j]+" ";
			}res+="\n";
		}
		boolean trouvee = false;
		int distanceBis = distance;
		if (!trouvee && xPersonnage > 0) {
			distanceBis = pathFinder[xPersonnage - 1][yPersonnage];
			
			if (distanceBis == distance - 1) {
				System.out.println(distance+" "+distanceBis+" "+"O");
				trouvee = true;
				direction='O';
				prochaine_case= this.laby.getCases()[xPersonnage-1][yPersonnage];
			} 
			
		} if (!trouvee && xPersonnage < pathFinder.length - 1) {
			distanceBis = pathFinder[xPersonnage + 1][yPersonnage];
			
			if (distanceBis == distance - 1) {
				System.out.println(distance+" "+distanceBis+"E");
				trouvee = true;
				direction = 'E';
				prochaine_case = this.laby.getCases()[xPersonnage+1][yPersonnage];
			}
		} if (!trouvee && yPersonnage > 0) {
			distanceBis = pathFinder[xPersonnage][yPersonnage-1];
			
			if (distanceBis == distance - 1) {
				System.out.println(distance+" "+distanceBis+"N");
				trouvee = true;
				direction='N';
				prochaine_case= this.laby.getCases()[xPersonnage][yPersonnage-1];
			} 
		}  if (!trouvee && yPersonnage < pathFinder[0].length - 1) {
			distanceBis = pathFinder[xPersonnage][yPersonnage + 1];
			
			if (distanceBis == distance - 1) {
				System.out.println(distance+" "+distanceBis+"S");
				trouvee = true;
				direction = 'S';
				prochaine_case = this.laby.getCases()[xPersonnage][yPersonnage+1];
			} p.setPosition(prochaine_case);
		} return direction;
	}
	
}
