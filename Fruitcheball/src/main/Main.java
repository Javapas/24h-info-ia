package main;

import java.io.IOException;
import java.net.UnknownHostException;

import labyrinthe.Labyrinthe;
import personnage.Equipe;

public class Main {

	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		String adresse = "127.0.0.1";
		String port = "1337";
		Labyrinthe labyrinthe ;
		if(args.length==2) {
			adresse=args[0];
			port=args[1];
		}
		
		System.out.println("Initialisation du client \n adresse : "+adresse+"\n port : "+port+"\n");
		Traitement t= new Traitement("127.0.0.1",Integer.parseInt(port));
		t.envoyerRep("jalu_java\n");
		String reponse = "";
		int numEquipe=0;
		Equipe equipe;
		//1er tour de boucle qui correspond � la recepetion du num�ro d'�quipe.
		boolean premierTour = true;
		//IATitou ia = new IATitou();
		IA ia;

		String message="";
		//boucle 
		while(reponse!="FIN") {
			reponse = t.attendreRep();
			//System.out.println(reponse);
			if(premierTour) {
				premierTour=false;
			}
			else {
				labyrinthe = new Labyrinthe(reponse);
				ia= new IA(labyrinthe);
				equipe = labyrinthe.getEquipes()[numEquipe];
				
				//System.out.println(labyrinthe);
				message=ia.deplacerEquipe(equipe);
				//String message = ia.message(labyrinthe);
				System.out.println(message);
				t.envoyerRep(message+"\n");
			}
		
		}
	}

}
