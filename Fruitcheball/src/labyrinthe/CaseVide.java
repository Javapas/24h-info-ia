package labyrinthe;

import java.awt.Point;

import fruit.Fruit;
import personnage.Personnage;

public class CaseVide extends Case {

	private Fruit fruit;
	
	public CaseVide(Point p) {
		super(p);
		fruit = null;
	}
	
	public CaseVide(Point p, boolean oqp) {
		super(p);
		fruit = null;
	}
	
	public CaseVide(Point p, Fruit f) {
		super(p);
		fruit = f;
	}
	
	public CaseVide(Point p, boolean oqp, Fruit f) {
		super(p);
		fruit = f;
	}
	
	public boolean estFranchissable() {
		return !estOccupee;
	}
	
	
	
	/**
	 * Returne vrai si la case poss�de un fruit
	 * @return
	 */
	public Fruit possederUnFruit(){
		return this.fruit;
	}
	

	/**
	 * Enlever Fruit de la case
	 * @return
	 */
	public void retirerFruit(){
		this.fruit=null;
	}
	
	/**
	 * Place un fruit sur la case
	 * @param f
	 */
	public void placerFruit(Fruit f){
		if(this.fruit==null){
			this.fruit=f;
		}
}	

	@Override
	public String toString() {
		Fruit f = this.possederUnFruit();
		if(this.estOccupee) return "j";
		if(f!=null) return f.toString();
		else return ".";
	}
}
