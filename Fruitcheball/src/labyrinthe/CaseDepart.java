package labyrinthe;

import java.awt.Point;

import personnage.Personnage;

public class CaseDepart extends Case{
	
	public CaseDepart(Point p) {
		super(p);
	}

	@Override
	public String toString() {
		return "o";
	}
}
