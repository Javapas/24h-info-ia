package labyrinthe;

import java.awt.Point;
import fruit.Fruit;
import personnage.Personnage;

public abstract class Case {

	private Point position;
	protected boolean estOccupee;
	
	public Case(Point p) {
		position = p;
		estOccupee = false;
	}
	
	public void setEstOccupee(boolean o) {
		estOccupee = o;
	}
	
	/**
	 * Indique si une case est traversable ou non
	 * @return 
	 */
	public boolean estFranchissable() {
		return true;
	}
	
	/**
	 * renvoie la position de la case
	 * @return position
	 */
	public Point getPosition(){
		return this.position;
	}
	
	/**
	 * Returne vrai si la case poss�de un fruit
	 * @return
	 */
	public Fruit possederUnFruit(){
		return null;
	}
	
	public abstract String toString();
	
	public boolean equals(Case c) {
		return c.getPosition().getX()==this.getPosition().getX() && c.getPosition().getY()==c.getPosition().getY();
	}
}
