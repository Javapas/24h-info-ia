package labyrinthe;

import java.awt.Point;
import java.util.ArrayList;

import fruit.*;
import personnage.Equipe;
import personnage.Lanceur;
import personnage.Personnage;
import personnage.Quetcherback;

public class Labyrinthe {

	private Case[][] cases;
	private Equipe[] equipes;

	/**
	 * Constructeur du jeu
	 * @param donnees Donn�es r�cup�r�es par le serveur
	 * @param numEquipe Num�ro de notre �quipe
	 */
	public Labyrinthe(String donnees) {
		String[] traitement = donnees.split("_");
		String[] lab = traitement[2].split(",");
		int notreEquipe = Integer.parseInt(traitement[0]);
		int nbEquipes = Integer.parseInt(traitement[1]);
		int colonne = Integer.parseInt(lab[0].split(":")[0]);
		int ligne = Integer.parseInt(lab[0].split(":")[1]);
		cases = new Case[colonne][ligne];
		equipes = new Equipe[nbEquipes];
		//Construction du labyrinthe
		//System.out.println("Labyrinthe");
		for(int x=0 ; x<colonne ; x++) {
			for(int y=0 ; y<ligne ; y++) {
				//Cloture
				if(lab[x+1].charAt(y)=='X') {
					cases[y][x] = new Cloture(new Point(y, x));
				}
				//Mirabelle
				else if(lab[x+1].charAt(y)=='0') {
					cases[y][x] = new CaseVide(new Point(y, x), new Mirabelle());
				}
				//Prune
				else if(lab[x+1].charAt(y)=='1') {
					cases[y][x] = new CaseVide(new Point(y, x), new Prune());
				}
				//Cerise
				else if(lab[x+1].charAt(y)=='2') {
					cases[y][x] = new CaseVide(new Point(y, x), new Cerise());
				}
				//Framboise
				else if(lab[x+1].charAt(y)=='3') {
					cases[y][x] = new CaseVide(new Point(y, x), new Framboise());
				}
				//Chataigne
				else if(lab[x+1].charAt(y)=='4') {
					cases[y][x] = new CaseVide(new Point(y, x), new Chataigne());
				}
				else {
					cases[y][x] = new CaseVide(new Point(y, x));
				}
			}
		}
		//Construction des �quipes
		for(int i=0 ; i<nbEquipes ; i++) {
			//System.out.println("Equipe "+i);
			String[] equipe = traitement[i+3].split(",");
			String[] chaine ;
			Equipe e = null ;
			if(i==notreEquipe) {
				e = new Equipe(i, true);
			}
			else {
				e = new Equipe(i, false);
			}
			for(int j=0 ; j<equipe.length ; j++) {
				chaine = equipe[j].split(":");
				//Construction de chaque joueur
				if(equipe[j].startsWith("P") && equipe[j].length()>1) {
					Case c = cases[Integer.parseInt(chaine[1])][Integer.parseInt(chaine[2])];
					c.setEstOccupee(true);
					Fruit f = null;
					switch (chaine[3]) {
					case "0": f = new Mirabelle();
					break;
					case "1": f = new Prune();
					break;
					case "2": f = new Cerise();
					break;
					case "3": f = new Framboise();
					break;
					case "4": f = new Chataigne();
					break;
					default:
						break;
					}
					//Quetcherback
					if(chaine[0].charAt(1)=='0') {
						//System.out.println("quetcherback");
						e.ajouterJoueur(new Quetcherback(c, f));
					}
					//Lanceur
					else {
						//System.out.println("lanceur");
						e.ajouterJoueur(new Lanceur(c, f));;
					}
				}
				//Construction des cases de d�part
				else if(equipe[j].startsWith("Z") && equipe[j].length()>1) {
					//System.out.println("case depart");
					Case c = cases[Integer.parseInt(chaine[2])][Integer.parseInt(chaine[1])];
					cases[Integer.parseInt(chaine[2])][Integer.parseInt(chaine[1])] = new CaseDepart(c.getPosition());
					e.ajouterCaseDepart((CaseDepart)cases[Integer.parseInt(chaine[2])][Integer.parseInt(chaine[1])]);
				}
				//Ajout du score
				else if(equipe[j].startsWith("G")) {
					//System.out.println("score");
					e.setScore(Integer.parseInt(equipe[j+1]));
				}
				//Ajout des fruits
				else if(equipe[j].startsWith("F") && equipe[j].length()>1) {
					//System.out.println("fruit");
					Fruit f = null;
					switch (chaine[0].charAt(1)) {
					case '0': f = new Mirabelle();
					break;
					case '1': f = new Prune();
					break;
					case '2': f = new Cerise();
					break;
					case '3': f = new Framboise();
					break;
					case '4': f = new Chataigne();
					break;
					default:
						break;
					}
					e.ajouterFruit(f, Integer.parseInt(chaine[1]));
				}
			}
			equipes[i] = e;
		}
	}

	public String toString() {
		String s = "";
		for(int x=0 ; x<cases.length ; x++) {
			for(int y=0 ; y<cases[x].length ; y++) {
				s+= cases[x][y].toString() + " | ";
			}
			s+= "\n";
		}
		
		return s;
	}

	/**
	 * methode permettant de generer le pathfinder en fonction d'une case donnee
	 * 
	 * @param destination
	 *            case ou l'on veut se rendre
	 * @return le pathfinder en fonction de cette case
	 */
	public int[][] getPathFinder(Case destination) {
		// On cree et initialise le tableau a -1
		int[][] pathFinder = new int[cases.length][cases[0].length];
		for (int i = 0; i < pathFinder.length; i++) {
			for (int j = 0; j < pathFinder[0].length; j++) {
				pathFinder[i][j] = -1;
			}
		}
		// On initialise la destination dans le pathFinder
		pathFinder[(int) destination.getPosition().getX()][(int) destination
		                                                   .getPosition().getY()] = 0;
		// l'index de la case dans le pathfinder
		int indexCase = 1;
		// Tant qu'on a pas fini de completer le pathfinder
		while (!this.estComplete(pathFinder)) {
			// On parcourt le tableau
			for (int ligne = 0; ligne < cases.length; ligne++) {
				for (int colonne = 0; colonne < cases[0].length; colonne++) {
					// Si on trouve une case dont l'index est le precedent
					// initialise
					if (pathFinder[ligne][colonne] == indexCase - 1) {
						// Si on est pas sur des bords
						if (ligne > 0)
							this.setNumeroCase(ligne - 1, colonne, indexCase,
									pathFinder);
						if (ligne < cases.length - 1)
							this.setNumeroCase(ligne + 1, colonne, indexCase,
									pathFinder);
						if (colonne > 0)
							this.setNumeroCase(ligne, colonne - 1, indexCase,
									pathFinder);
						if (colonne < cases[0].length - 1)
							this.setNumeroCase(ligne, colonne + 1, indexCase,
									pathFinder);
					}
				}
			}
			indexCase++; // On incremente l'index
		}

		return pathFinder;
	}

	/**
	 * methode attribuant un numero a une case du pathfinder
	 * 
	 * @param ligne
	 *            ligne de la case
	 * @param colonne
	 *            colonne de la case
	 * @param indexCase
	 *            index a mettre sur la case
	 * @param pathFinder
	 *            pathfinder a modifier
	 */
	private void setNumeroCase(int ligne, int colonne, int indexCase,
			int[][] pathFinder) {
		// si la case est franchissable et non initialisee alors on initialise
		if (cases[ligne][colonne].estFranchissable()
				&& pathFinder[ligne][colonne] == -1)
			pathFinder[ligne][colonne] = indexCase;
	}

	/**
	 * methode permettant de savoir si le tableau est complete
	 * 
	 * @param pathFinder
	 *            pathfinder a verifier
	 * @return true si le pathfinder est complete
	 */
	private boolean estComplete(int[][] pathFinder) {
		boolean complet = true;
		// Pour chaque case dans le tableau
		for (int ligne = 0; ligne < cases.length; ligne++) {
			for (int colonne = 0; colonne < cases[0].length; colonne++) {
				// Si une case n'est pas initialisee on arrete la boucle
				if (pathFinder[ligne][colonne] == -1
						&& cases[ligne][colonne].estFranchissable()) {
					complet = false;
					break;
				}
			}
			if (!complet)
				break;
		}
		return complet;
	}

	/**
	 * @return le tableau de cases du labyrinthe
	 */
	public Case[][] getCases(){
		return this.cases;
	}

	public Equipe[] getEquipes() {
		return this.equipes;
	}

	

}


