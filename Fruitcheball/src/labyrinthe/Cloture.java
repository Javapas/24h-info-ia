package labyrinthe;

import java.awt.Point;

public class Cloture extends Case {

	private Point position;
	
	public Cloture(Point p) {
		super(p);
	}
	
	/**
	 * Indique si une cas est traversable ou non
	 */
	public boolean estFranchissable() {
		return false;
	}

	@Override
	public String toString() {
		return "X";
	}
}
